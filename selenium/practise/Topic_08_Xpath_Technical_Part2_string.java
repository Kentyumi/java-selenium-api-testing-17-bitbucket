package practise;

public class Topic_08_Xpath_Technical_Part2_string {

	public static void main(String[] args) {
       String name = "Selenium Webdrive API";
       System.out.println(name.contains("Selenium"));
       System.out.println(name.contains("Webdrive"));
       System.out.println(name.contains("API"));
       System.out.println(name.contains("ium Webd"));
       
       System.out.println(name.contains("Grid"));
       System.out.println(name.equals("Selenium"));
       System.out.println(name.equals("Webdrive"));
       System.out.println(name.equals("API"));
       System.out.println(name.equals("Selenium Webdrive API"));
       
       String selenium = "Selenium ";
       String automation = "Automation Framework";
       System.out.println(selenium.concat(automation));
       
	}

}
